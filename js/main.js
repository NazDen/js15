function stopWatch(elem) {
    let time = 0
    let interval
    let offset

    function update() {
        if (this.isOn) {
            time += delta()
        }
        let formattedTime = timeFormatter(time)
        elem.textContent = formattedTime
    }

    function delta() {
        let now = Date.now()
        let timePassed = now - offset
        offset = now
        return timePassed
    }

    function timeFormatter(timeInMilliseconds) {
        let time = new Date(timeInMilliseconds)
        let minutes = time.getMinutes().toString()
        let seconds = time.getSeconds().toString()
        let milliseconds = time.getMilliseconds().toString()
        if (minutes.length < 2) {
            minutes = '0' + minutes
        }
        if (seconds.length < 2) {
            seconds = '0' + seconds
        }

        while (milliseconds.length < 3) {
            milliseconds = '0' + milliseconds
        }
        return minutes + ' : ' + seconds + ' : ' + milliseconds
    }

    this.isOn = false

    this.start = function () {
        if (!this.isOn) {
            interval = setInterval(update.bind(this), 10)
            offset = Date.now()
            this.isOn = true
        }
    }
    this.pause = function () {
        if (this.isOn) {
            clearInterval(interval)
            interval = null
            this.isOn = false
        }
    }
    this.clear = function () {
        this.pause()
        time = 0
        update()

    }
}
let timer = document.getElementById('timer')
let sP = document.getElementById('startPause')
let clear = document.getElementById('Clear')
let watch = new stopWatch(timer)

sP.addEventListener('click', function () {
    if (watch.isOn) {
        watch.pause()
        sP.textContent = 'Start'
    } else {
        watch.start()
        sP.textContent = 'Pause'
    }
})
clear.addEventListener('click', function () {
    watch.clear()
    sP.textContent = 'Start'
})
